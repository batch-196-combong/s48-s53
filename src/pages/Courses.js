import {useState, useEffect} from 'react'
import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	// checks if the mock data is captured
	// console.log(courseData);
	// console.log(courseData[0])

	const [courses, setCourses] = useState([]);

	useEffect(()=>{
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return (
					<CourseCard key={course.id} courseProp={course}/>
				)
			}))
		})
	},[])

	// const courses = courseData.map(course =>{
	// 	console.log(course)
	// 	return(
	// 		<CourseCard key={course.id} courseProp = {course}/>
	// 	)
	// })

	return(
		<>
			<h1>Available Courses</h1>
			{courses}
		</>
	)
}